package mb.gp.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.InvalidParameterException;

import static mb.gp.math.CompareResult.EQUAL;
import static mb.gp.math.CompareResult.GREATER;


public class Fraction implements Comparable<Fraction>
{

  private final BigInteger numerator;
  private final BigInteger denominator;

  public static final Fraction ZERO = new Fraction(BigInteger.ZERO, BigInteger.ONE);
  public static final Fraction ONE = new Fraction(BigInteger.ONE, BigInteger.ONE);
  public static final Fraction TWO = new Fraction(BigInteger.valueOf(2), BigInteger.ONE);
  public static final Fraction TEN = new Fraction(BigInteger.TEN, BigInteger.ONE);


  public static Fraction valueOf(BigInteger value)
  {
    return new Fraction(value, BigInteger.ONE);
  }


  public static Fraction valueOf(Number value)
  {
    if (value != null)
    {
      return valueOf(BigInteger.valueOf(value.longValue()));
    }
    return valueOf(null);
  }


  public Fraction(BigInteger numerator, BigInteger denominator)
  {
    if (numerator == null || denominator == null || denominator.compareTo(BigInteger.ZERO) < GREATER)
    {
      throw new InvalidParameterException("Must provide numerator and non zero denominator");
    }
    this.numerator = numerator;
    this.denominator = denominator;
  }


  public Fraction(long numerator, long denominator)
  {
    this(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
  }


  @Override
  public int compareTo(Fraction fraction)
  {
    if (fraction == null)
    {
      return GREATER;
    }

    BigInteger result = this.numerator.multiply(fraction.denominator);
    result = result.subtract(this.denominator.multiply(fraction.numerator));

    return result.compareTo(BigInteger.ZERO);
  }


  @Override public boolean equals(Object o)
  {
    if (o instanceof Fraction)
    {
      return this.compareTo((Fraction) o) == EQUAL;
    }
    return (o instanceof Number) && this.compareTo(Fraction.valueOf((Number) o)) == EQUAL;
  }


  @Override public int hashCode()
  {
    return super.hashCode();
  }


  public Fraction add(Fraction fraction)
  {
    if (fraction == null)
    {
      return this.reduce();
    }

    BigInteger newDenominator = findCommonDenominator(this.denominator, fraction.denominator);

    BigInteger newNumerator = this.numerator.multiply(newDenominator.divide(this.denominator));
    newNumerator = newNumerator.add(fraction.numerator.multiply(newDenominator.divide(fraction.denominator)));

    return new Fraction(newNumerator, newDenominator).reduce();
  }


  public Fraction subtract(Fraction fraction)
  {
    if (fraction == null)
    {
      return this.reduce();
    }

    BigInteger newDenominator = findCommonDenominator(this.denominator, fraction.denominator);

    BigInteger newNumerator = this.numerator.multiply(newDenominator.divide(this.denominator));
    newNumerator = newNumerator.subtract(fraction.numerator.multiply(newDenominator.divide(fraction.denominator)));

    return new Fraction(newNumerator, newDenominator).reduce();
  }


  public Fraction divide(Fraction fraction)
  {
    if (fraction == null)
    {
      return null;
    }

    BigInteger newNumerator = this.numerator.multiply(fraction.denominator);
    BigInteger newDenominator = this.denominator.multiply(fraction.numerator.abs());

    return new Fraction(newNumerator, newDenominator).reduce();
  }


  public Fraction multiply(Fraction fraction)
  {
    if (fraction == null)
    {
      return null;
    }

    BigInteger newNumerator = this.numerator.multiply(fraction.numerator);
    BigInteger newDenominator = this.denominator.multiply(fraction.denominator);

    return new Fraction(newNumerator, newDenominator).reduce();
  }


  public Fraction reverse()
  {
    return new Fraction(denominator, numerator);
  }


  public Fraction reduce()
  {
    BigInteger commonFactor = findGreatestCommonDivisor(numerator, denominator);
    return new Fraction(numerator.divide(commonFactor), denominator.divide(commonFactor));
  }


  public BigDecimal toBigDecimal(int scale, RoundingMode roundingMode)
  {
    BigDecimal decimalNumerator = new BigDecimal(numerator.toString());
    BigDecimal decimalDenominator = new BigDecimal(denominator.toString());
    return decimalNumerator.divide(decimalDenominator, scale, roundingMode);
  }


  public String toString()
  {
    return String.format("%s/%s", numerator.toString(), denominator.toString());
  }


  public BigInteger getNumerator()
  {
    return numerator;
  }


  public BigInteger getDenominator()
  {
    return denominator;
  }


  private BigInteger findCommonDenominator(BigInteger denominatorA, BigInteger denominatorB)
  {
    BigInteger commonDenominator = findGreatestCommonDivisor(denominatorA, denominatorB);
    commonDenominator = (denominatorA.multiply(denominatorB)).divide(commonDenominator);

    return commonDenominator;
  }


  private BigInteger findGreatestCommonDivisor(BigInteger argA, BigInteger argB)
  {
    if (argA.equals(BigInteger.ZERO))
    {
      return argB.abs();
    }
    return findGreatestCommonDivisor(argB.remainder(argA), argA).abs();
  }

}
