package mb.gp.math;

public final class CompareResult
{

  public static final int LOWER = -1;
  public static final int EQUAL = 0;
  public static final int GREATER = 1;

  private CompareResult()
  {
  }

}
