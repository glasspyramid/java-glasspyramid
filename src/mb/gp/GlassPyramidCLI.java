package mb.gp;

import mb.gp.glasspyramid.Glass;
import mb.gp.glasspyramid.GlassId;
import mb.gp.glasspyramid.GlassPyramid;
import mb.gp.math.Fraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.RoundingMode;

class GlassPyramidCLI
{

  private static final String QUIT_CODE = "q";
  private static final String FRACTIONAL_CODE = "b";
  private static final String DECIMAL_CODE = "d";

  private final BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

  private boolean fractionalMode = false;


  @SuppressWarnings("squid:S106")
  void run()
  {
    writeIntro();

    while (true)
    {
      GlassId glassId = readRequiredGlass();

      if (glassId == null)
      {
        break;
      }

      StringBuilder errorMessage = new StringBuilder();

      if (GlassPyramid.validateGlassId(glassId, errorMessage))
      {
        Glass requestedGlass = GlassPyramid.SINGLETON.getGlass(glassId);
        writeSpillOverTime(requestedGlass.startsToSpillOverAt());
      }
      else
      {
        System.out.println(errorMessage);
      }
    }

  }


  @SuppressWarnings("squid:S106")
  private void writeIntro()
  {
    System.out.println("Välkommen till glaspyramiden");
    System.out.println(DECIMAL_CODE + " = Presentera decimaltal");
    System.out.println(FRACTIONAL_CODE + " = Presentera bråktal");
    System.out.println(QUIT_CODE + " = Avsluta");
    System.out.println();
  }


  @SuppressWarnings("squid:S106")
  private void writeSpillOverTime(Fraction startsToSpillOverAt)
  {
    if (fractionalMode)
    {
      System.out.printf("Det tar %s sekunder.", startsToSpillOverAt);
    }
    else
    {
      System.out.printf("Det tar %s sekunder.", startsToSpillOverAt.toBigDecimal(3, RoundingMode.HALF_EVEN));
    }

    System.out.println("");
    System.out.println("");
  }


  private GlassId readRequiredGlass()
  {
    GlassId result = null;

    Integer row = promptForIntegerValue("Rad ? ");

    if (row != null)
    {
      Integer glass = promptForIntegerValue("Glas ? ");

      if (glass != null)
      {
        result = new GlassId(row, glass);
      }
    }

    return result;
  }


  @SuppressWarnings("squid:S106")
  private Integer promptForIntegerValue(String prompt)
  {
    Integer result = null;

    do
    {
      System.out.print(prompt);
      String input = readFromConsole();

      if (input == null || input.equalsIgnoreCase(QUIT_CODE))
      {
        break;
      }

      if (input.equalsIgnoreCase(FRACTIONAL_CODE))
      {
        setFractionalMode(true);
      }
      if (input.equalsIgnoreCase(DECIMAL_CODE))
      {
        setFractionalMode(false);
      }
      else
      {
        result = stringToInteger(input);
      }

    }
    while (result == null);

    return result;
  }


  @SuppressWarnings("squid:S106")
  private void setFractionalMode(boolean fractionalModeOn)
  {
    fractionalMode = fractionalModeOn;

    if (fractionalMode)
    {
      System.out.println("Visar bråktal");
    }
    else
    {
      System.out.println("Visar decimaltal");
    }
  }


  private String readFromConsole()
  {
    String result;

    try
    {
      result = console.readLine();
    }
    catch (IOException e)
    {
      return null;
    }

    if (result != null)
    {
      result = result.trim();
    }

    return result;
  }


  private Integer stringToInteger(String string)
  {
    try
    {
      return Integer.valueOf(string);
    }
    catch (NumberFormatException e)
    {
      return null;
    }
  }

}
