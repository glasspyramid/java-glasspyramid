package mb.gp.glasspyramid;

import mb.gp.glasspyramid.speed.SpeedChange;
import mb.gp.glasspyramid.speed.SpeedChangeSequence;
import mb.gp.math.Fraction;

import static mb.gp.math.CompareResult.GREATER;

public class Glass
{

  // The pyramid starts to fill up att time 0
  private static final Fraction INITIAL_POINT_IN_TIME = Fraction.ZERO;
  // The top glass fills up with the speed 10 time-units / glass
  private static final Fraction TOP_GLASS_FILL_SPEED = Fraction.TEN;

  private final int rowIndex;
  private final int glassIndex;

  private final SpeedChange deliverySpeed;


  Glass(int rowIndex, int glassIndex)
  {
    this.rowIndex = rowIndex;
    this.glassIndex = glassIndex;

    deliverySpeed = calculateDeliverySpeed();
  }


  public Fraction startsToSpillOverAt()
  {
    return deliverySpeed.getWhen();
  }


  private SpeedChange calculateDeliverySpeed()
  {
    SpeedChangeSequence parentDeliverySpeeds = calculateParentDeliverySpeeds();
    SpeedChange firstSpeedChange = parentDeliverySpeeds.getFirstSpeedChange();
    SpeedChange secondSpeedChange = parentDeliverySpeeds.getSecondSpeedChange();

    Fraction glassFillLevel = Fraction.ZERO;
    Fraction glassWillFillUpAt = calculatePointInTimeWhenGlassWillFillUp(glassFillLevel, firstSpeedChange);

    if
      (
      (secondSpeedChange == null) ||
      (secondSpeedChange.getWhen().compareTo(glassWillFillUpAt) >= GREATER)
      )
    {
      return new SpeedChange(glassWillFillUpAt, firstSpeedChange.fillSpeedToDeliverySpeed());
    }

    glassFillLevel = increaseGlassFillLevel(glassFillLevel, firstSpeedChange, secondSpeedChange);
    glassWillFillUpAt = calculatePointInTimeWhenGlassWillFillUp(glassFillLevel, secondSpeedChange);

    return new SpeedChange(glassWillFillUpAt, secondSpeedChange.fillSpeedToDeliverySpeed());
  }


  private SpeedChangeSequence calculateParentDeliverySpeeds()
  {
    Glass parentLeft = GlassPyramid.SINGLETON.getGlass(rowIndex - 1, glassIndex - 1);
    Glass parentRight = GlassPyramid.SINGLETON.getGlass(rowIndex - 1, glassIndex);

    if (parentLeft != null && parentRight != null)
    {
      return new SpeedChangeSequence(parentLeft.getDeliverySpeed(), parentRight.getDeliverySpeed());
    }
    if (parentLeft != null)
    {
      return new SpeedChangeSequence(parentLeft.getDeliverySpeed());
    }
    if (parentRight != null)
    {
      return new SpeedChangeSequence(parentRight.getDeliverySpeed());
    }

    // A glass without parents is the top most glass in the pyramid
    return new SpeedChangeSequence(new SpeedChange(INITIAL_POINT_IN_TIME, TOP_GLASS_FILL_SPEED));
  }


  private SpeedChange getDeliverySpeed()
  {
    return deliverySpeed;
  }


  private Fraction calculatePointInTimeWhenGlassWillFillUp(Fraction glassFillLevel, SpeedChange fillSpeed)
  {
    Fraction remainsToFillUp = Fraction.ONE.subtract(glassFillLevel);
    Fraction glassWillFillUpIn = fillSpeed.getSpeed().multiply(remainsToFillUp);
    return fillSpeed.getWhen().add(glassWillFillUpIn);
  }


  private Fraction increaseGlassFillLevel(Fraction currentFillLevel, SpeedChange currentSpeed, SpeedChange nextSpeed)
  {
    Fraction timeReceivingAtCurrentSpeed = nextSpeed.getWhen().subtract(currentSpeed.getWhen());
    Fraction volumeReceivedAtCurrentSpeed = timeReceivingAtCurrentSpeed.divide(currentSpeed.getSpeed());
    return currentFillLevel.add(volumeReceivedAtCurrentSpeed);
  }

}
