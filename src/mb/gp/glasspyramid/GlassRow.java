package mb.gp.glasspyramid;

import java.util.HashMap;
import java.util.Map;

class GlassRow
{

  private final int rowIndex;
  private final Map<Integer, Glass> glasses;


  static boolean isValidGlassIndex(GlassId glassId)
  {
    return (1 <= glassId.glassIndex()) && (glassId.glassIndex() <= glassId.rowIndex());
  }


  GlassRow(int rowIndex)
  {
    this.rowIndex = rowIndex;
    glasses = new HashMap<>();
  }


  Glass getGlass(int glassIndex)
  {
    if (!isValidGlassIndex(glassIndex))
    {
      return null;
    }
    return glasses.computeIfAbsent(glassIndex, key -> new Glass(rowIndex, glassIndex));
  }


  private boolean isValidGlassIndex(int glassIndex)
  {
    return isValidGlassIndex(new GlassId(rowIndex, glassIndex));
  }

}