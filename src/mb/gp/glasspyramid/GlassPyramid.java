package mb.gp.glasspyramid;

import java.util.HashMap;
import java.util.Map;

public enum GlassPyramid
{

  SINGLETON;

  private static final int MAX_ROW = 500;

  private Map<Integer, GlassRow> glassRows;


  GlassPyramid()
  {
    glassRows = new HashMap<>();
  }


  public Glass getGlass(int rowIndex, int glassIndex)
  {
    if (!isValidRowIndex(rowIndex))
    {
      return null;
    }

    GlassRow requestedRow = glassRows.computeIfAbsent(rowIndex, GlassRow::new);

    return requestedRow.getGlass(glassIndex);
  }


  public Glass getGlass(GlassId glassId)
  {
    return getGlass(glassId.rowIndex(), glassId.glassIndex());
  }


  public static boolean validateGlassId(GlassId glassId, StringBuilder errorMessage)
  {
    boolean result = false;

    if (!isValidRowIndex(glassId.rowIndex()))
    {
      errorMessage.append("1 <= row <= 500");
    }
    else if (!GlassRow.isValidGlassIndex(glassId))
    {
      errorMessage.append("1 <= glass <= row");
    }
    else
    {
      result = true;
    }

    return result;
  }


  private static boolean isValidRowIndex(int index)
  {
    return (1 <= index) && (index <= MAX_ROW);
  }

}
