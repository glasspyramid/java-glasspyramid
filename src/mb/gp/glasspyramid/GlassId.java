package mb.gp.glasspyramid;

public class GlassId
{
  
  private final int rowIndex;
  private final int glassIndex;


  public GlassId(int rowIndex, int glassIndex)
  {
    this.rowIndex = rowIndex;
    this.glassIndex = glassIndex;
  }


  public int rowIndex()
  {
    return rowIndex;
  }


  public int glassIndex()
  {
    return glassIndex;
  }

}
