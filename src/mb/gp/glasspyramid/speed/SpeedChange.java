package mb.gp.glasspyramid.speed;

import mb.gp.math.Fraction;

public class SpeedChange
{

  private final Fraction when;
  private final Fraction speed;


  public SpeedChange(Fraction when, Fraction speed)
  {
    this.when = when;
    this.speed = speed;
  }


  public Fraction getWhen()
  {
    return when;
  }


  public Fraction getSpeed()
  {
    return speed;
  }


  public SpeedChange addSpeed(Fraction additionalSpeed)
  {
    return new SpeedChange(when, Speed.combineSpeeds(speed, additionalSpeed));
  }


  public Fraction fillSpeedToDeliverySpeed()
  {
    return Speed.fillSpeedToDeliverySpeed(speed);
  }

}