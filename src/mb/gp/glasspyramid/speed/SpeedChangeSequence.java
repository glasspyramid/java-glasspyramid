package mb.gp.glasspyramid.speed;

import java.security.InvalidParameterException;

import static mb.gp.math.CompareResult.GREATER;
import static mb.gp.math.CompareResult.LOWER;


public class SpeedChangeSequence
{

  private final SpeedChange firstSpeedChange;
  private final SpeedChange secondSpeedChange;


  public SpeedChangeSequence(SpeedChange speedChangeA, SpeedChange speedChangeB)
  {
    if (speedChangeA == null || speedChangeB == null)
    {
      throw new InvalidParameterException("Both speed change's must be supplied");
    }

    int compareValue = speedChangeA.getWhen().compareTo(speedChangeB.getWhen());

    if (compareValue <= LOWER)
    {
      firstSpeedChange = speedChangeA;
      secondSpeedChange = speedChangeB.addSpeed(speedChangeA.getSpeed());
    }
    else if (compareValue >= GREATER)
    {
      firstSpeedChange = speedChangeB;
      secondSpeedChange = speedChangeA.addSpeed(speedChangeB.getSpeed());
    }
    else
    {
      firstSpeedChange = speedChangeA.addSpeed(speedChangeB.getSpeed());
      secondSpeedChange = null;
    }
  }


  public SpeedChangeSequence(SpeedChange speedChange)
  {
    if (speedChange == null)
    {
      throw new InvalidParameterException("A speed change must be supplied");
    }
    firstSpeedChange = speedChange;
    secondSpeedChange = null;
  }


  public SpeedChange getFirstSpeedChange()
  {
    return firstSpeedChange;
  }


  public SpeedChange getSecondSpeedChange()
  {
    return secondSpeedChange;
  }

}
