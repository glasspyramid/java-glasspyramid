package mb.gp.glasspyramid.speed;

import mb.gp.math.Fraction;

public class Speed
{

  // Speed is expressed as time/glass.
  // Fill speed is the speed with which a glass i filling up. The top glass fills up with 10/1.
  // Delivery speed is the speed with which a glass delivers to one of its two child glasses. The top glass hence delivers at the speed 20/1.

  static Fraction combineSpeeds(Fraction speed1, Fraction speed2)
  {
    return speed1.reverse().add(speed2.reverse()).reverse();
  }

  static Fraction fillSpeedToDeliverySpeed(Fraction fillSpeed)
  {
    return fillSpeed.multiply(Fraction.TWO);
  }

}
